 Redirect Extensions

This module extends the Redirect module (https://www.drupal.org/project/redirect) 

### Extensions ###

* Adds tracking of 'Created By', 'Created Date', 'Updated By', 'Updated Date'.
* Adds the ability to perform bulk updates on status code and destination URL.

### Requirements ###

* Drupal 8.2.x or higher
* The Redirect contrib module (https://www.drupal.org/project/redirect) 

### Installation ###

* Make sure the Redirect module is enabled. 
* Install as usual as per http://drupal.org/node/895232.
