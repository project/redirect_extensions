<?php

namespace Drupal\Tests\redirect_extensions\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\redirect_extensions\RedirectDatabaseStorage;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the RedirectDatabaseStorage class.
 *
 * @coversDefaultClass \Drupal\redirect_extensions\RedirectDatabaseStorage
 * @group redirect_extensions
 */
class RedirectDatabaseStorageTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::redirectExists
   * @dataProvider addDataProvider
   */
  public function testRedirectExists($count, $expected) {

    // Create mocks for the Drupal classes used by testRedirect;.
    $connectionProphecy = $this->prophesize(Connection::class);
    $accountProphecy = $this->prophesize(AccountInterface::class);
    $timeProphecy = $this->prophesize(TimeInterface::class);
    $queryProphecy = $this->prophesize(SelectInterface::class);
    $countQueryProphecy = $this->prophesize(SelectInterface::class);
    $resultProphecy = $this->prophesize(StatementInterface::class);

    $redirect_id = "1";

    $resultProphecy->rowCount()->willReturn($count);

    $queryProphecy->fields('r')->shouldBeCalledTimes(1)
      ->willReturn($queryProphecy->reveal());
    $queryProphecy->condition('rid', $redirect_id, '=')->shouldBeCalledTimes(1)
      ->willReturn($queryProphecy->reveal());
    $queryProphecy->countQuery()->shouldBeCalledTimes(1)
      ->willReturn($countQueryProphecy->reveal());
    $countQueryProphecy->execute()->shouldBeCalledTimes(1)
      ->willReturn($resultProphecy->reveal());
    $resultProphecy->fetchField()->shouldBeCalledTimes(1)
      ->willReturn($count);

    $connectionProphecy->select("redirect_extensions", "r")
      ->willReturn($queryProphecy->reveal());

    $redirectStorage = new RedirectDatabaseStorage($connectionProphecy->reveal(),
      $accountProphecy->reveal(), $timeProphecy->reveal());

    self::assertEquals($expected, $redirectStorage->redirectExists($redirect_id));

  }

  /**
   * Data provider for test.
   */
  public static function addDataProvider() {
    return [
      [0, FALSE],
      [1, TRUE],
      [2, TRUE],
    ];
  }

}
