<?php

namespace Drupal\redirect_extensions;

use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Class RedirectDatabaseStorage.
 */
class RedirectDatabaseStorage implements RedirectDatabaseStorageInterface {

  /**
   * Drupal\mysql\Driver\Database\mysql\Connection definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * The time interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new RedirectDatabaseStorage object.
   *
   * @param \Drupal\mysql\Driver\Database\mysql\Connection $database
   *   The Database connection.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time class.
   */
  public function __construct(Connection $database,
  AccountInterface $current_user,
              TimeInterface $time) {
    $this->database = $database;
    $this->user = $current_user;
    $this->time = $time;
  }

  /**
   * Insert a redirect.
   *
   * @param string $redirect_id
   *   New redirect ID.
   */
  public function insertRedirect($redirect_id) {

    // Save current UNIX timestamp.
    $date = $this->time->getRequestTime();

    $record = [
      'rid' => $redirect_id,
      'uid_created' => $this->user->id(),
      'created' => $date,
      'modified' => $date,
    ];

    $this->database->insert('redirect_extensions')->fields($record)->execute();
  }

  /**
   * Check if redirect id exists.
   *
   * @param string $redirect_id
   *   Redirect ID to be checked.
   *
   * @return bool
   *   True if redirect exists.
   */
  public function redirectExists($redirect_id) {
    $result = $this->database->select('redirect_extensions', 'r')
      ->fields('r')
      ->condition('rid', $redirect_id, '=')
      ->countQuery()
      ->execute()
      ->fetchField();
    return !empty($result) && $result > 0;
  }

  /**
   * Update a redirect.
   *
   * @param string $redirect_id
   *   ID of redirect being updated.
   */
  public function updateRedirect($redirect_id) {

    // Save current UNIX timestamp.
    $date = $this->time->getRequestTime();

    $record = [
      'uid_created' => $this->user->id(),
      'modified' => $date,
    ];

    $this->database->update('redirect_extensions')
      ->fields($record)
      ->condition('rid', $redirect_id, '=')
      ->execute();
  }

  /**
   * Delete a redirect.
   *
   * @param string $redirect_id
   *   ID of redirect being deleted.
   */
  public function deleteRedirect($redirect_id) {
    $this->database->delete('redirect_extensions')
      ->condition('rid', $redirect_id, '=')
      ->execute();

  }

}
